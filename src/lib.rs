//! Macros for testing `nom` parsers
//!
//! Often when I'm testing `nom` parsers, I end up defining a lot of little
//! macros like this, so I thought I would bundle them all up into a crate
//! so I didn't have to define them over and over.
//!
//! This crate was first created back when nom had the concept of "Done" vs "Finished",
//! which might seem a little out of place now but I still find it useful when testing.
//! Basically, macros that test for "Done" check that the parser completed
//! successfully, while macros that test for "Finished" check that the parser
//! completed successfully while also asserting that the input is empty.
//!
#![deny(missing_docs)] // <- this doesn't appear to check macros?

/// Instead of importing the helper macros individually, this can be
/// star-imported to get all of them
pub mod prelude {
    pub use crate::{
        assert_done,
        assert_finished,
        assert_done_and_eq,
        assert_finished_and_eq,
        assert_error,
        assert_error_and_eq,
        assert_needed,
        assert_needs,
    };
}

#[macro_export]
/// This macro checks to make sure that the IResult it is
/// passed is `Done`. That is, it checks that the parser completed successfully
/// but doesn't make any assumptions about the remaining input.
///
/// # Examples
///
/// ```
/// use nom_test_helpers::assert_done;
/// use nom::{named, tag};
///
/// # fn main() {
/// named!(abcd<&str, &str>, tag!("abcd"));
/// let r = abcd("abcd");
/// assert_done!(r);
/// # }
/// ```
macro_rules! assert_done {
    ($e:expr $(,)?) => (
        assert_done!($e, "parser did not complete");
    );
    ($e:expr, $($arg:tt)+) => ({
            if let ::std::result::Result::Ok((_, _)) = $e {
                assert!(true);
            } else {
                assert!(false, $($arg)+);
            }
    })
}

#[cfg(test)]
mod assert_done_test {
    use super::prelude::*;

    #[test]
    fn no_msg() {
        let result: nom::IResult<&str, &str> = Ok(("foo", "bar"));
        assert_done!(result);
    }

    #[test]
    fn with_msg() {
        let result: nom::IResult<&str, &str> = Ok(("foo", "bar"));
        assert_done!(result, "should be done");
    }

    #[test]
    #[should_panic]
    fn panic_no_msg() {
        let result: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_done!(result,);
    }

    #[test]
    #[should_panic]
    fn panic_with_msg() {
        let result: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_done!(result, "should be done");
    }
}

#[macro_export]
/// This does the same thing as `assert_done!`, except that
/// this also asserts that the input slice is empty
///
/// # Examples
///
/// ```
/// use nom_test_helpers::assert_finished;
///
/// # fn main() {
/// let r: nom::IResult<&str, &str> = Ok(("", "efgh"));
/// assert_finished!(r);
/// # }
/// ```
macro_rules! assert_finished {
    ($e:expr $(,)?) => {
        assert_finished!($e, "parser did not complete");
    };
    ($e:expr, $($arg:tt)+) => ({
        if let ::std::result::Result::Ok((i, _)) = $e {
            assert!(i.is_empty());
        } else {
            assert!(false, $($arg)+);
        }
    })
}

#[cfg(test)]
mod assert_finished_tests {
    use super::prelude::*;

    #[test]
    fn no_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_finished!(r);
    }

    #[test]
    fn with_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_finished!(r, "should be finished");
    }

    #[test]
    #[should_panic]
    fn panic_ok_no_msg() {
        let r: nom::IResult<&str, &str> = Ok((" ", "foo"));
        assert_finished!(r);
    }

    #[test]
    #[should_panic]
    fn panic_ok_with_msg() {
        let r: nom::IResult<&str, &str> = Ok((" ", "foo"));
        assert_finished!(r, "should be finished");
    }

    #[test]
    #[should_panic]
    fn panic_err_no_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_finished!(r);
    }

    #[test]
    #[should_panic]
    fn panic_err_with_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_finished!(r, "should be finished");
    }
}

#[macro_export]
/// This checks that the `IResult` is `Done`, and lets you
/// check that the value returned as the `O` type of the 
/// `IResult` is equal to the second parameter
///
/// # Examples
///
/// ```
/// use nom_test_helpers::assert_done_and_eq;
///
/// # fn main() {
/// let r: nom::IResult<&[u8], &[u8]> = Ok((b"abcd", b"efgh"));
/// assert_done_and_eq!(r, b"efgh");
/// # }
/// ```
macro_rules! assert_done_and_eq {
    ($e:expr, $a:expr $(,)?) => {
        assert_done_and_eq!($e, $a, "parser did not complete");
    };
    ($e:expr, $a:expr, $($arg:tt)+) => ({
        if let ::std::result::Result::Ok((_, o)) = $e {
            assert_eq!(o, $a);
        } else {
            assert!(false, $($arg)+);
        }
    });
}

#[cfg(test)]
mod assert_done_and_eq_tests {
    use super::prelude::*;

    #[test]
    fn no_msg() {
        let r: nom::IResult<&str, &str> = Ok((" ", "foo"));
        assert_done_and_eq!(r, "foo");
    }

    #[test]
    fn with_msg() {
        let r: nom::IResult<&str, &str> = Ok((" ", "foo"));
        assert_done_and_eq!(r, "foo", "should be done");
    }

    #[test]
    #[should_panic]
    fn panic_no_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_done_and_eq!(r, "foo");
    }

    #[test]
    #[should_panic]
    fn panic_with_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_done_and_eq!(r, "foo", "should be done");
    }
}

#[macro_export]
/// Same as `assert_done_and_eq!`, but asserts that
/// the input slice is empty
///
/// # Examples
///
/// ```
/// use nom_test_helpers::assert_finished_and_eq;
///
/// # fn main() {
/// let r: nom::IResult<&str, &str> = Ok(("", "sup"));
/// assert_finished_and_eq!(r, "sup");
/// # }
/// ```
macro_rules! assert_finished_and_eq {
    ($r:expr, $o:expr $(,)?) => {
        assert_finished_and_eq!($r, $o, "parser did not complete");
    };
    ($r:expr, $o:expr, $($arg:tt)+) => ({
        if let ::std::result::Result::Ok((i, o)) = $r {
            assert!(i.is_empty());
            assert_eq!(o, $o);
        } else {
            assert!(false, $($arg)+);
        }
    })
}

#[cfg(test)]
mod assert_finished_and_eq_tests {
    use super::prelude::*;

    #[test]
    fn no_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_finished_and_eq!(r, "foo");
    }

    #[test]
    fn with_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_finished_and_eq!(r, "foo", "should be finished");
    }

    #[test]
    #[should_panic]
    fn panic_ok_no_msg() {
        let r: nom::IResult<&str, &str> = Ok((" ", "foo"));
        assert_finished_and_eq!(r, "foo");
    }

    #[test]
    #[should_panic]
    fn panic_ok_with_msg() {
        let r: nom::IResult<&str, &str> = Ok((" ", "foo"));
        assert_finished_and_eq!(r, "foo", "should be finished");
    }

    #[test]
    #[should_panic]
    fn panic_err_no_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_finished_and_eq!(r, "foo");
    }

    #[test]
    #[should_panic]
    fn panic_err_with_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_finished_and_eq!(r, "foo", "should be finished");
    }
}

#[macro_export]
/// This asserts that the `IResult` is an `Err`
/// 
/// # Examples
/// 
/// ```
/// use nom_test_helpers::assert_error;
///
/// # fn main() {
/// let r: nom::IResult<&[u8], &[u8]> = Err(nom::Err::Error(nom::error::Error::new(&b""[..], nom::error::ErrorKind::Count)));
/// assert_error!(r);
/// # }
macro_rules! assert_error {
    ($e:expr $(,)?) => {
        assert_error!($e, "parser did not error");
    };
    ($e:expr, $($arg:tt)+) => ({
        if let ::std::result::Result::Err(_) = $e {
            assert!(true);
        } else {
            assert!(false, $($arg)+);
        }
    })
}

#[cfg(test)]
mod assert_error_tests {
    use super::prelude::*;

    #[test]
    fn no_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_error!(r);
    }

    #[test]
    fn with_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_error!(r, "should be an error");
    }

    #[test]
    #[should_panic]
    fn panic_no_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_error!(r);
    }

    #[test]
    #[should_panic]
    fn panic_with_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_error!(r, "should be an error");
    }
}

#[macro_export]
/// This asserts that the `IResult` is an `Err` and that the error
/// is what is expected
///
/// # Examples
///
/// ```
/// use nom_test_helpers::assert_error_and_eq;
///
/// # fn main() {
/// let r: nom::IResult<&[u8], &[u8]> = Err(nom::Err::Error(nom::error::Error::new(&b""[..], nom::error::ErrorKind::Count)));
/// assert_error_and_eq!(r, nom::Err::Error(nom::error::Error::new(&b""[..], nom::error::ErrorKind::Count)));
/// # }
macro_rules! assert_error_and_eq {
    ($r:expr, $err:expr $(,)?) => {
        assert_error_and_eq!($r, $err, "parser did not error");
    };
    ($r:expr, $err:expr, $($arg:tt)+) => ({
        if let ::std::result::Result::Err(err) = $r {
            assert_eq!(err, $err);
        } else {
            assert!(false, $($arg)+);
        }
    })
}

#[cfg(test)]
mod assert_error_and_eq_tests {
    use super::prelude::*;

    #[test]
    fn no_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_error_and_eq!(r, nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
    }

    #[test]
    fn with_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
        assert_error_and_eq!(r, nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)), "should be an error");
    }

    #[test]
    #[should_panic]
    fn panic_no_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_error_and_eq!(r, nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)));
    }

    #[test]
    #[should_panic]
    fn panic_with_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_error_and_eq!(r, nom::Err::Error(nom::error::Error::new("", nom::error::ErrorKind::Count)), "should be an error");
    }
}

#[macro_export]
/// This asserts that the `IResult` is an `Incomplete`
///
/// # Examples
///
/// ```
/// use std::num::NonZeroUsize;
/// use nom_test_helpers::assert_needed;
/// use nom::Needed;
///
/// # fn main() {
/// let r: nom::IResult<&[u8], &[u8]> = Err(nom::Err::Incomplete(Needed::Size(NonZeroUsize::new(1).unwrap())));
/// assert_needed!(r);
/// # }
/// ```
macro_rules! assert_needed {
    ($e:expr $(,)?) => {
        assert_needed!($e, "parser is not incomplete");
    };
    ($e:expr, $($arg:tt)+) => ({
        if let ::std::result::Result::Err(::nom::Err::Incomplete(..)) = $e {
            assert!(true);
        } else {
            assert!(false, $($arg)+);
        }
    })
}

#[cfg(test)]
mod assert_needed_tests {
    use super::prelude::*;

    #[test]
    fn no_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Incomplete(nom::Needed::Unknown));
        assert_needed!(r);
    }

    #[test]
    fn with_msg() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Incomplete(nom::Needed::Unknown));
        assert_needed!(r, "should be Needed");
    }

    #[test]
    #[should_panic]
    fn panic_no_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_needed!(r);
    }

    #[test]
    #[should_panic]
    fn panic_with_msg() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_needed!(r, "should be Needed");
    }
}

#[macro_export]
/// This lets the user specify how much input the parser should need
///
/// # Examples
///
/// ```
/// use nom_test_helpers::assert_needs;
/// use nom::Needed;
///
/// # fn main() {
/// let r: nom::IResult<&[u8], &[u8]> = Err(nom::Err::Incomplete(Needed::Unknown));
/// assert_needs!(r, ?);
/// # }
/// ```
///
/// ```
/// use std::num::NonZeroUsize;
/// use nom_test_helpers::assert_needs;
/// use nom::Needed;
///
/// # fn main() {
/// let r: nom::IResult<&[u8], &[u8]> = Err(nom::Err::Incomplete(Needed::Size(NonZeroUsize::new(2).unwrap())));
/// assert_needs!(r, 2usize);
/// # }
/// ```
macro_rules! assert_needs {
    ($e:expr, ? $(,)? ) => {
        if let ::std::result::Result::Err(::nom::Err::Incomplete(e)) = $e {
            if let ::nom::Needed::Unknown = e {
                assert!(true);
            } else {
                assert!(false, "parser is incomplete, but Needed is known");
            }
        } else {
            assert!(false, "parser is not incomplete");
        }
    };
    ($e:expr, ?, $($arg:tt)+) => {
        if let ::std::result::Result::Err(::nom::Err::Incomplete(e)) = $e {
            if let ::nom::Needed::Unknown = e {
                assert!(true);
            } else {
                assert!(false, $($arg)+);
            }
        } else {
            assert!(false, $($arg)+);
        }
    };

    ($e:expr, $i:expr $(,)?) => {
        if let ::std::result::Result::Err(::nom::Err::Incomplete(e)) = $e {
            if let ::nom::Needed::Size(i) = e {
                assert_eq!($i, i.into());
            } else {
                assert!(false, "parser is incomplete, but Needed is unknown");
            }
        } else {
            assert!(false, "parser is not incomplete");
        }
    };
    ($e:expr, $i:expr, $($arg:tt)+) => ({
        if let ::std::result::Result::Err(::nom::Err::Incomplete(e)) = $e {
            if let ::nom::Needed::Size(i) = e {
                assert_eq!($i, i.into());
            } else {
                assert!(false, $($arg)+);
            }
        } else {
            assert!(false, $($arg)+);
        }
    });
}

#[cfg(test)]
mod assert_needs_tests {
    use super::prelude::*;
    use std::num::NonZeroUsize;

    #[test]
    fn no_msg_size() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Incomplete(nom::Needed::Size(NonZeroUsize::new(2).unwrap())));
        assert_needs!(r, 2usize);
    }

    #[test]
    fn no_msg_unknown() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Incomplete(nom::Needed::Unknown));
        assert_needs!(r, ?);
    }

    #[test]
    fn with_msg_size() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Incomplete(nom::Needed::Size(NonZeroUsize::new(2).unwrap())));
        assert_needs!(r, 2usize, "should be Needed(2)");
    }

    #[test]
    fn with_msg_unknown() {
        let r: nom::IResult<&str, &str> = Err(nom::Err::Incomplete(nom::Needed::Unknown));
        assert_needs!(r, ?, "should be Needed(Unknown)");
    }

    #[test]
    #[should_panic]
    fn panic_no_msg_size() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_needs!(r, 2usize);
    }

    #[test]
    #[should_panic]
    fn panic_no_msg_unknown() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_needs!(r, ?);
    }

    #[test]
    #[should_panic]
    fn panic_with_msg_unknown() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_needs!(r, ?, "Should be Needed(Unknown)");
    }

    #[test]
    #[should_panic]
    fn panic_with_msg_size() {
        let r: nom::IResult<&str, &str> = Ok(("", "foo"));
        assert_needs!(r, 2usize, "Should be Needed(2)");
    }
}
